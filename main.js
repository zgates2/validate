// Dependencies
// ======
const testValidityOfInput = require("./helpers").testValidityOfInput;
const checkForHyphen = require("./helpers").checkForHyphen;
const countTheHyphens = require("./helpers").countTheHyphens;
const verifyDate = require("./helpers").verifyDate;
const verifyTime = require("./helpers").verifyTime;
const fs = require("fs");

// Declarations
// ======
const validInput = [];
const containsHyphen = [];
const countHyphen = [];
const dateStructure = [];
const labeledOutput = [];
const timeStructure = [];

// Runtime
// ============

//Set array input
let arr = ["text", 20, "testset@@@", "2021-5-21", 255, "23:23:23"];

//Iterate through each item in the array
for (let i = 0; i < arr.length; i++) {
  // Perform each of our checks
  containsHyphen.push(checkForHyphen(arr[i]));
  validInput.push(testValidityOfInput(arr[i]));
  countHyphen.push(countTheHyphens(arr[i]));
  timeStructure.push(verifyTime(arr[i]));
  dateStructure.push(verifyDate(arr[i]));
  // Build Report Object
  const objPayload = {
    dataInput: arr[i],
    validInput: validInput[i],
    hasHyphenCheck: containsHyphen[i],
    hyphenCount: countHyphen[i],
    validDateFormat: dateStructure[i],
    validTimeFormat: timeStructure[i],
  };
  labeledOutput.push(objPayload);
}

console.log(labeledOutput[0]);

const header = Object.keys(labeledOutput[0]);

console.log(header);

const fileName = "output2.csv";
const data = [];

data.push(header.join(","));

for (let i = 0; i < labeledOutput.length; i++) {
  const row = [];
  for (let j = 0; j < header.length; j++) {
    console.log(1, labeledOutput[i][header[j]]);
    console.log(2, header[j]);
    row.push(labeledOutput[i][header[j]]);
  }
  console.log(row);
  data.push(row.join(","));
}

console.log(data.join("\n"));

fs.writeFileSync(fileName, data.join("\n"));
